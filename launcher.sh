#!/bin/sh

SOURCEPORT_EXEC="./darkplaces-linux-x86_64-glx"
MOD_DIR=(*/)
CURRENT_DIR=$(pwd)
RADIO_STATE=()
ARGS=()

get_mods () {
    for (( i = 0; i < "${#MOD_DIR[@]}"; ++i )); do
        ARGS[2*i]="${RADIO_STATE[i]}"
        ARGS[2*i + 1]="${MOD_DIR[i]%?}"
    done
}

select_mod () {
    MOD=$( zenity \
        --list --radiolist \
        --title="Quake Mod Launcher" --text="Source Port: ${SOURCEPORT_EXEC}" \
        --column="Select" --column="Mod" \
        --height 300 \
        --width 300 \
        "${ARGS[@]}" \
    )
    
    if [ "$?" -eq 1 ]
    then
        exit 1
    else
        echo "Directory: ${CURRENT_DIR}"
        echo "Executable: ${SOURCEPORT_EXEC}"
        echo "Modification: ${MOD}"

        if [ -z "$MOD" ]
        then
            exec "${CURRENT_DIR}/${SOURCEPORT_EXEC}"
        else
            exec "${CURRENT_DIR}/${SOURCEPORT_EXEC}" -game "${MOD}"
        fi
    fi
}

get_mods
select_mod
