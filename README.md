# Quake Mod Launcher

A dead simple linux shell Zenity based Quake 1 mod/expansion launcher in 44 lines of pure shell awesomeness

**Requirements:**
*  A Quake 1 Sourceport (DarkPlaces: https://icculus.org/twilight/darkplaces/files/)
*  zenity (https://wiki.gnome.org/action/show/Projects/Zenity)
*  Quake retail or demo (Shareware: https://www.moddb.com/games/quake/downloads/quake-shareware-106)
*  Quake mods/expansions (Quake 1.5: https://www.moddb.com/mods/quake-15)

**Instructions**
1.  Install dependencies
2.  Clone this repo
3.  By default this script is set to run DarkPlaces SourcePort (replace "./darkplaces-linux-x86_64-glx" with prefered SourcePort executable path filename)
 
**Screenshots**

![](screenshots/2020-03-28-164746_374x348_scrot.png)